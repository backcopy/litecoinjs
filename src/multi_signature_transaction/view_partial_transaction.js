/*
MODULE NAME: view_partial_transaction.js
PURPOSE: View information about partially signed multi-sig
transactions.
EXPORTS:
    (a) viewPartialMultiSignatureTransaction()
NOTES: No notes.
*/

const litecoinjs = require('../../lib/core/index.js')
/*
NETWORK: 'normal' or 'testnet'

RAWTRANSACTION: The raw transaction.
*/
async function viewPartialMultiSignatureTransaction (network, rawTransaction) {
  return new Promise(async (resolve, reject) => {
    try {
      // Set litecoin network
      const litecoinNetwork = (network === 'normal' ? litecoinjs.networks.litecoin : litecoinjs.networks.testnet)
      // Retrieve transaction
      const txb = litecoinjs.TransactionBuilder.fromTransaction(litecoinjs.Transaction.fromHex(rawTransaction), litecoinNetwork)
      resolve(txb)
    } catch (error) {
      return reject(new Error(`ERROR IN [viewPartialMultiSignatureTransaction] MAIN CATCH BLOCK: ${error}`))
    }
  })
}

module.exports = viewPartialMultiSignatureTransaction
