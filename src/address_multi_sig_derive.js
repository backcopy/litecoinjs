/*
MODULE NAME: address_multi_sig_derive.js
PURPOSE: Generate multi signature litecoin addresses using provided public
keys.
DEPENDENCIES:
    (a) /lib/core/index.js
EXPORTS:
    (a) deriveLitecoinAddressMultiSig(publicKeys, signatures, network)
NOTES:
    (a) The wallet import format (WIF) is an encoded version of
    the private key.
*/

const litecoinjs = require('../lib/core/index.js')
/*
PUBLICKEYS: An array with the public keys used that can sign transactions,
including the index.
    (1) [{
        publickey: '03156c0aeaf32475618d58061fb0f11a3b4d3bb9e663fecfb53e6661a8ca9b0116',
        index: 0
        },
        {
        publickey: '0341e31bf8c5984b54dd579c2d14da9c2d0283fb1c81e149b6223fd5465d6dbf48',
        index: 1
        }]
SIGNATURES: The number of signatures required to execute transactions from this address.
NETWORK: The network, normal or testnet.
*/

function deriveLitecoinAddressMultiSig (publicKeys, signatures, network = 'testnet') {
  return new Promise((resolve, reject) => {
    try {
      // Set litecoin specific configuration.
      const litecoinNetwork = network === 'normal' ? litecoinjs.networks.litecoin : litecoinjs.networks.testnet
      // Convert publicKey to Buffer
      let pubKeysBuffer = publicKeys.map(function (individualPublicKey) {
        return Buffer.from(individualPublicKey.publicKey, 'hex')
      })

      const redeemScript = litecoinjs.script.multisig.output.encode(signatures, pubKeysBuffer) // number of sigs
      const scriptPubKey = litecoinjs.script.scriptHash.output.encode(litecoinjs.crypto.hash160(redeemScript))
      let address = litecoinjs.address.fromOutputScript(scriptPubKey, litecoinNetwork)
      // Convert address to current SegWit encoding standard (TESTNET)
      address = network === 'normal' ? address : litecoinjs.address.toBase58Check(litecoinjs.address.fromBase58Check(address)['hash'], 58)

      resolve({ master: address, witnessScript: redeemScript.toString('hex') })
    } catch (error) {
      reject(new Error(`ERROR IN [deriveLitecoinAddressMultiSig] MAIN CATCH BLOCK: ${error}`))
    }
  })
}

module.exports = deriveLitecoinAddressMultiSig
