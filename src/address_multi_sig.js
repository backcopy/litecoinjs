/*
MODULE NAME: address_multi_sig.js
PURPOSE: Generate multi signature litecoin addresses.
DEPENDENCIES:
    (a) /lib/core/index.js
EXPORTS:
    (a) createLitecoinAddressMultiSig(addresses, signatures, network)
NOTES:
    (a) The wallet import format (WIF) is an encoded version of
    the private key.
*/

const litecoinjs = require('../lib/core/index.js')
/*
ADDRESSES: The number of total addresses that can sign from this address.
SIGNATURES: The number of signatures required to execute transactions from this address.
NETWORK: The network, normal or testnet.
*/

function createLitecoinAddressMultiSig (addresses, signatures, network = 'testnet') {
  return new Promise((resolve, reject) => {
    try {
      // Set litecoin specific configuration.
      const litecoinNetwork = network === 'normal' ? litecoinjs.networks.litecoin : litecoinjs.networks.testnet
      let keysVector = []
      let pubKeyVector = []

      // Generate addresses
      for (let index = 0; index < addresses; index++) {
        const keyPair = litecoinjs.ECPair.makeRandom({ network: litecoinNetwork })
        const wif = keyPair.toWIF()
        const keyPairRaw = litecoinjs.ECPair.fromWIF(wif, litecoinNetwork)
        const pubKeyBuffer = keyPairRaw.getPublicKeyBuffer()
        const publicKey = pubKeyBuffer.toString('hex')

        keysVector.push({ wif, publicKey, index })
        pubKeyVector.push(pubKeyBuffer)
      }

      // Multi sig address generation
      let pubKeysBuffer = pubKeyVector.map(function (hex) {
        return Buffer.from(hex, 'hex')
      })

      const witnessScript = litecoinjs.script.multisig.output.encode(signatures, pubKeysBuffer) // number of signatures
      const witnessScriptHash = litecoinjs.crypto.sha256(witnessScript)
      const redeemScript = litecoinjs.script.witnessScriptHash.output.encode(witnessScriptHash)
      const redeemScriptHash = litecoinjs.crypto.hash160(redeemScript)
      const scriptPubKey = litecoinjs.script.scriptHash.output.encode(redeemScriptHash)

      // const scriptPubKey = litecoinjs.script.scriptHash.output.encode(litecoinjs.crypto.hash160(redeemScript))
      let address = litecoinjs.address.fromOutputScript(scriptPubKey, litecoinNetwork)
      // Convert address to current SegWit encoding standard (TESTNET)
      address = network === 'normal' ? address : litecoinjs.address.toBase58Check(litecoinjs.address.fromBase58Check(address)['hash'], 58)

      resolve({
        master: address,
        witnessScript: witnessScript.toString('hex'),
        keys: keysVector
      })
    } catch (error) {
      reject(new Error(`ERROR IN [createLitecoinAddressMultiSig] MAIN CATCH BLOCK: ${error}`))
    }
  })
}

module.exports = createLitecoinAddressMultiSig
