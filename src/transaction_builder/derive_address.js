/*
MODULE NAME: derive_address.js
PURPOSE: Derive addresses from a WIF.
DEPENDENCIES:
    (a) /lib/core/index.js
EXPORTS:
    (a) deriveAddress()
NOTES:
    (a) This function will derive a address using the provided WIF.
    (b) Testnet addresses will use old-encoding for compatibility reasons,
    i.e start segwit testnet addresses will start with 2 instead of Q.
*/

const litecoinjs = require('../../lib/core/index.js')

function deriveAddress (wif, wifType) {
  return new Promise((resolve, reject) => {
    try {
      if (wifType === 'normal') {
        const keyPair = litecoinjs.ECPair.fromWIF(wif, litecoinjs.networks.litecoin)
        const pubKey = keyPair.getPublicKeyBuffer()

        const redeemScript = litecoinjs.script.witnessPubKeyHash.output.encode(litecoinjs.crypto.hash160(pubKey))
        const scriptPubKey = litecoinjs.script.scriptHash.output.encode(litecoinjs.crypto.hash160(redeemScript))

        let address = litecoinjs.address.fromOutputScript(scriptPubKey, litecoinjs.networks.litecoin)
        address = litecoinjs.address.toBase58Check(litecoinjs.address.fromBase58Check(address)['hash'], 50)
        resolve(address)
      } else if (wifType === 'testnet') {
        const keyPair = litecoinjs.ECPair.fromWIF(wif, litecoinjs.networks.testnet)
        const pubKey = keyPair.getPublicKeyBuffer()

        const redeemScript = litecoinjs.script.witnessPubKeyHash.output.encode(litecoinjs.crypto.hash160(pubKey))
        const scriptPubKey = litecoinjs.script.scriptHash.output.encode(litecoinjs.crypto.hash160(redeemScript))

        let address = litecoinjs.address.fromOutputScript(scriptPubKey, litecoinjs.networks.testnet)
        address = litecoinjs.address.toBase58Check(litecoinjs.address.fromBase58Check(address)['hash'], 196)
        resolve(address)
      } else {
        reject(new TypeError('Invalid wifType specified in [deriveAddress] function'))
      }
    } catch (error) {
      reject(new Error(`ERROR IN [deriveAddress] MAIN CATCH BLOCK: ${error}`))
    }
  })
};

module.exports = deriveAddress
